FROM ubuntu:20.04
RUN apt-get -y update && \
    apt-get install -y wget && \
    wget -O /bin/hadolint https://github.com/hadolint/hadolint/releases/download/v1.16.3/hadolint-Linux-x86_64 &&\
    chmod +x /bin/hadolint

CMD ["/bin/hadolint", "-"]

